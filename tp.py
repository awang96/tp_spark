from pyspark import SparkContext
from pyspark.sql import SparkSession
import pyspark.sql.functions as F
from pyspark.sql.functions import udf
import math

sc = SparkContext()

spark = SparkSession.builder.appName('meteo').getOrCreate()


class Commune:
    DEPCOM = 'DEPCOM'
    PTOT = 'PTOT'

    @classmethod
    def read(cls):
        depcom = cls.DEPCOM
        ptot = cls.PTOT
        return spark.read.csv('Communes.csv', header=True, sep=';')\
            .rdd\
            .map(lambda x: (x[depcom], x[ptot])).toDF()\
            .withColumnRenamed('_1', depcom)\
            .withColumnRenamed('_2', ptot)


class Departement:
    CODE_INSEE = 'CODE INSEE'
    CODE_DEPT = 'Code Dept'
    GEOM_X_Y = 'geom_x_y'

    @classmethod
    def read(cls):
        code_insee = cls.CODE_INSEE
        code_dept = cls.CODE_DEPT
        geom = cls.GEOM_X_Y
        return spark.read.csv('code-insee-postaux-geoflar.csv', header=True, sep=';')\
            .rdd.map(lambda x: (x[code_insee], x[code_dept], x[geom])).toDF()\
            .withColumnRenamed('_1', code_insee)\
            .withColumnRenamed('_2', code_dept)\
            .withColumnRenamed('_3', geom)


class Poste:
    ID = 'ID'
    LATITUDE = 'Latitude'
    LONGITUDE = 'Longitude'

    @classmethod
    def read(cls):
        id = cls.ID
        latitude = cls.LATITUDE
        longitude = cls.LONGITUDE
        return spark.read.csv('postesSynop.txt', header=True, sep=';')\
            .rdd.map(lambda x: (x[id], x[latitude], x[longitude])).toDF()\
            .withColumnRenamed('_1', id)\
            .withColumnRenamed('_2', latitude)\
            .withColumnRenamed('_3', longitude)


class Synop:
    T = 't'
    NUMER_STA = 'numer_sta'
    DATE = 'date'

    @classmethod
    def read(cls):
        t = cls.T
        numer_sta = cls.NUMER_STA
        date = cls.DATE
        return spark.read.csv('synop.2020120512.txt', header=True, sep=';')\
            .rdd.map(lambda x: (x[t], x[numer_sta], x[date])).toDF()\
            .withColumnRenamed('_1', t)\
            .withColumnRenamed('_2', numer_sta)\
            .withColumnRenamed('_3', date)


@udf('float')
def k_to_c(x):
    return float(x) - 273.15 if x != 'mq' else 0


@udf('string')
def min_distance(latitude, longitude):
    distances = [(math.sqrt((float(x[0][0]) - float(latitude)) ** 2 + (float(x[0][1]) - float(longitude)) ** 2), x[1])
                 if x else None for x in l]
    min_dist, dpt = distances[0]
    for x in distances:
        if x and x[0] < min_dist:
            min_dist, dpt = x
    return dpt


df_commune = Commune.read()
# df_commune.show(10)

df_dpt = Departement.read()
# df_dpt.show(10)

df_poste = Poste.read()
# df_poste.show(10)

df_synop = Synop.read()
# df_synop.show(10)

df_dpt_com = df_commune.withColumnRenamed(Commune.DEPCOM, Departement.CODE_INSEE)\
    .join(
        df_dpt,
        Departement.CODE_INSEE
    )
# df_dpt_com.show()

df_dpt_ptot = df_dpt_com.groupBy(Departement.CODE_DEPT).agg(
    F.sum(df_dpt_com[Commune.PTOT]).alias('population')
)
# df_dpt_ptot.show()

df_poste_synop = df_poste.withColumnRenamed(Poste.ID, Synop.NUMER_STA)\
    .join(df_synop, Synop.NUMER_STA)\
    .withColumn('t_c', k_to_c(df_synop[Synop.T]))
# df_poste_synop.show()

l = [(x[Departement.GEOM_X_Y].split(','), x[Departement.CODE_INSEE]) if x[Departement.GEOM_X_Y] else None for x in df_dpt_com.rdd.collect()]
# print(commune_geom_list)

df_poste_synop_insee = df_poste_synop\
    .withColumn(Departement.CODE_INSEE, min_distance(df_poste_synop[Poste.LATITUDE],
                                                     df_poste_synop[Poste.LONGITUDE]))
df_poste_synop_insee.show()
